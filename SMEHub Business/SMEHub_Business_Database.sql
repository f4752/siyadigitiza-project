-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 17, 2022 at 01:53 PM
-- Server version: 10.3.37-MariaDB
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `focalsoft_smehubedward`
--

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `id` int(11) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `email` varchar(225) NOT NULL,
  `tel` varchar(225) NOT NULL,
  `website` varchar(225) NOT NULL,
  `dp` varchar(225) NOT NULL,
  `owner` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`id`, `business_name`, `category`, `email`, `tel`, `website`, `dp`, `owner`) VALUES
(2, 'Focalsoft', 'Information Technology', 'info@focalsoft.co.za', '031 825 3831', 'www.focalsoft.co.za', '1650886396-download.jpg', '0812762691'),
(3, 'Maluju Oils', 'Construction', 'info@malujuoils.co.za', '0737656127', 'www.malujuoils.co.za', '1650460046-toyota.jpg', '0737656127'),
(4, 'Mahumana Industries', 'Manufacturing', 'info@mindustries.org', '0731112344', 'www.mindustries.org', '1650543271-Google.jpg', '0731112344'),
(5, 'Sizwe house', 'Computers & Electronics', 'jilasizwegigolo@gmail.com', '0738836795', 'www.nobuhle.co.za', '1650622948-rKRkBAv__400x400.jpg', '0738836795'),
(6, 'Nobuhle Media', 'Entertainment', 'info@nobuhlemedia.co.za', '0310000000', 'www.nobuhlemedia.co.za', '1650888629-1650543271-Google.jpg', '0723126044'),
(7, 'Makwa Auto', 'Automotive', 'makwa@gmail.com', '07951112555', 'N/A', '', '0712321122'),
(8, 'Rond Vista', 'Legal & Financial', 'test@gmail.com', '0737656126', 'N/A', '1668630965-episode-5-jenny-okonkwo-mentoring-black-female-accounting-professionals.jpg', '0737656126'),
(9, 'Lenovo', 'Computers & Electronics', 'edwardson@gmail.com', '0813109193', 'www.lenovo.com', '', '0813109193'),
(10, 'TSN Data', 'Computers & Electronics', 'mesuli@focalsoft.co.za', '0843156594', 'www.tsndata.co.za', '', '0843156594'),
(11, 'Nobuhle Media', 'Entertainment', 'edward@nobuhlemedia.co.za', '0719442777', 'www.nobuhlemedia.co.za', '', '0719442777'),
(12, 'The Legacy', 'Construction & Contractors', 'annastac@gmail.com', '0723126040', 'www.thelegacy.co.za', '1668674121-images.png', '12'),
(13, 'Ukhozi FM', 'Computers & Electronics', 'ukhozi@gmail.com', '0893109193', 'www.ukhozi.co.za', '1668677279-VL79VXhAhb.jpg', '13'),
(14, 'John Doe Enterprise', 'Automotive', 'edwards@gmails.com', '0653768442', 'www.jdenterprise.co.za', '1668680684-download.jpg', '14');

-- --------------------------------------------------------

--
-- Table structure for table `business_view`
--

CREATE TABLE `business_view` (
  `id` int(11) NOT NULL,
  `business` varchar(225) NOT NULL,
  `viewer` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `business_view`
--

INSERT INTO `business_view` (`id`, `business`, `viewer`) VALUES
(1, '0737656127', '0812762691'),
(2, '0731112344', '0812762691'),
(3, '0737656127', '0812762691'),
(4, '0738836795', '0812762691'),
(5, '0737656127', '0812762691'),
(6, '0737656127', '0812762691'),
(7, '0738836795', '0812762691'),
(8, '0812762691', '0723126044'),
(9, '0738836795', '0723126044'),
(10, '0812762691', '0723126044'),
(11, '0723126044', '0812762691'),
(12, '0737656127', '0812762691'),
(13, '0731112344', '0812762691'),
(14, '0723126044', '0812762691'),
(15, '0731112344', '0812762691'),
(16, '0731112344', '0812762691'),
(17, '0737656127', '0812762691'),
(18, '0723126044', '0812762691'),
(19, '0737656127', '0812762691'),
(20, '0737656127', '0812762691'),
(21, '0738836795', '0812762691'),
(22, '0737656127', '0812762691'),
(23, '0723126044', '0812762691'),
(24, '0731112344', '0812762691'),
(25, '0738836795', '0812762691'),
(26, '0812762691', '0737656126');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `item` varchar(225) NOT NULL,
  `price` varchar(225) NOT NULL,
  `quantity` varchar(225) NOT NULL,
  `buyer` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `compliance`
--

CREATE TABLE `compliance` (
  `id` int(11) NOT NULL,
  `registration` varchar(225) NOT NULL,
  `bank` varchar(225) NOT NULL,
  `tax` varchar(225) NOT NULL,
  `bee` varchar(225) NOT NULL,
  `owner` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `compliance`
--

INSERT INTO `compliance` (`id`, `registration`, `bank`, `tax`, `bee`, `owner`) VALUES
(1, '1', '1', '1', '1', '0737656126'),
(2, '1', '1', '1', '1', '0813109193'),
(3, '0', '0', '0', '0', '0843156594'),
(4, '1', '1', '1', '1', '0719442777'),
(5, '1', '1', '1', '1', '12'),
(6, '1', '1', '1', '1', '13'),
(8, '1', '1', '1', '1', '14');

-- --------------------------------------------------------

--
-- Table structure for table `dp`
--

CREATE TABLE `dp` (
  `id` int(11) NOT NULL,
  `profile_image` varchar(225) NOT NULL,
  `bio` varchar(225) NOT NULL,
  `owners` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `dp`
--

INSERT INTO `dp` (`id`, `profile_image`, `bio`, `owners`) VALUES
(25, '1650452816-istockphoto-1140705087-170667a.jpg', '', '0812762691'),
(42, '1650455801-toyota.jpg', 'Toyota Motor Corporation is a Japanese multinational automotive manufacturer headquartered in Toyota City, Aichi, Japan.', '0812762691'),
(43, '1650458282-istockphoto-1140705087-170667a.jpg', '', '0812762691'),
(44, '1650458597-istockphoto-1140705087-170667a.jpg', '', '0812762691'),
(45, '1650459997-toyota.jpg', '', '0812762691'),
(46, '1650460022-toyota.jpg', '', '0812762691'),
(47, '1650460024-toyota.jpg', '', '0812762691'),
(48, '1650460046-toyota.jpg', '', '0737656127'),
(49, '1650460065-toyota.jpg', '', '0812762691'),
(50, '1650460067-toyota.jpg', '', '0812762691'),
(51, '1650460078-istockphoto-1140705087-170667a.jpg', '', '0812762691'),
(52, '1650460104-istockphoto-1140705087-170667a.jpg', '', '0812762691'),
(53, '1650460164-istockphoto-1140705087-170667a.jpg', '', '0812762691'),
(54, '1650460179-istockphoto-1140705087-170667a.jpg', '', '0812762691'),
(55, '1650460226-istockphoto-1140705087-170667a.jpg', '', '0812762691'),
(56, '1650532207-rKRkBAv__400x400.jpg', '', '0731112344'),
(57, '1650543271-Google.jpg', '', '0731112344'),
(58, '1650622948-rKRkBAv__400x400.jpg', '', '0738836795'),
(59, '1650881740-1650543271-Google.jpg', '', '0812762691'),
(60, '1650886396-download.jpg', '', '0812762691'),
(61, '1650888629-1650543271-Google.jpg', '', '0723126044'),
(62, '1668542588-instagram (1).png', '', '0737656126'),
(63, '1668630948-icon.png', '', '0737656126'),
(64, '1668630965-episode-5-jenny-okonkwo-mentoring-black-female-accounting-professionals.jpg', '', '0737656126'),
(65, '1668673982-images.png', '', '12'),
(66, '1668674121-images.png', '', '12'),
(67, '1668677279-VL79VXhAhb.jpg', '', '13'),
(68, '1668680684-download.jpg', '', '14');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `item_name` varchar(225) NOT NULL,
  `item_category` varchar(225) NOT NULL,
  `item_price` varchar(225) NOT NULL,
  `payment_type` varchar(225) NOT NULL,
  `item_gallery` varchar(225) NOT NULL,
  `item_owner` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_name`, `item_category`, `item_price`, `payment_type`, `item_gallery`, `item_owner`) VALUES
(3, 'Beauty Comb', 'Beauty', '450', 'Once-Off', 'toyota.jpg', '0812762691'),
(4, 'Beauty Comb', 'Beauty', '250', 'Once-Off', 'peakpx.jpg', '0812762691'),
(5, 'dsasdsda', 'Beauty', '200', 'Once-Off', '125-1255075_thriller-images-thriller-hd-wallpaper-and-background-thriller.jpg', '0812762691'),
(6, 'sadsaddsa', 'Beauty', '500', 'Once-Off', '258754903_171402205244159_1012643981755236080_n.jpg', '0812762691'),
(7, 'sdaasdsda', 'Beauty', '400', 'Once-Off', '', '0812762691'),
(8, 'sdaasdsadsa', 'Baby And Toddler', '500', 'Once-Off', 'peakpx (1).jpg', '0812762691'),
(9, 'cvbcvvbcvcb', 'Beauty', '400', 'Once-Off', 'rKRkBAv__400x400.jpg', '0738836795'),
(10, 'Lenovo Laptop', 'Computers And Tablets', '7500', 'Once-Off', '1650543271-Google.jpg', '0723126044'),
(11, 'dassda', 'Beauty', '200', 'Per Month', 'Ukuphila BNS Logo.jpg', '0812762691'),
(12, 'asedsada', 'Books', '320', 'Once-Off', '', '0812762691'),
(13, 'Item One', 'Books', '100', 'Once-Off', 'episode-5-jenny-okonkwo-mentoring-black-female-accounting-professionals.jpg', '0737656126'),
(14, 'Pink Lilies', 'Computers And Tablets', '500', 'Once-Off', 'pink-lilies-roses-in-vase-500.jpg', '12'),
(15, 'Lilies Rose Flower', 'Gaming', '500', 'Once-Off', 'pink-lilies-roses-in-vase-500.jpg', '14');

-- --------------------------------------------------------

--
-- Table structure for table `item_view`
--

CREATE TABLE `item_view` (
  `id` int(11) NOT NULL,
  `item` varchar(225) NOT NULL,
  `viewer` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `item_view`
--

INSERT INTO `item_view` (`id`, `item`, `viewer`) VALUES
(1, '10', '0812762691'),
(2, '10', '0812762691'),
(3, '9', '0812762691'),
(4, '10', '0812762691'),
(5, '9', '0812762691'),
(6, '9', '0812762691'),
(7, '9', '0812762691');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `memberID` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `active` varchar(255) NOT NULL,
  `resetToken` varchar(255) DEFAULT NULL,
  `resetComplete` varchar(3) DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`memberID`, `username`, `password`, `email`, `active`, `resetToken`, `resetComplete`) VALUES
(1, '0812762691', '$2y$10$W6PkQu.pHPZWJEK4Qa4z1OPXa2fYyimxF51vidJVIWeLRRgNOEq2S', 'edwardthabekhulu@gmail.com', 'Yes', NULL, 'No'),
(2, '0723126044', '$2y$10$0EE1KoSyD3h/D1tN/lP/7.QxuTgEFlSrc4sl5NkCTxl/79H9o0kj.', 'edward@focalsoft.co.za', 'Yes', NULL, 'No'),
(3, '0737656127', '$2y$10$X9wChycEM8MICrDN0s/4bee66PGtk/1DwTcguxX/c2sOCyU8hMpIu', 'annamacave@gmail.com', 'Yes', NULL, 'No'),
(4, '0731112344', '$2y$10$skBFQIB/iOFIEbMXOc4.OO9LBmwJSqYaCf/Ti34leBBJeQPj/H/Cq', 'mralbinomahumana@gmail.com', 'Yes', NULL, 'No'),
(5, '0738836795', '$2y$10$1m7AopA2xdr0VhDrCOQpCOilAvxoPkBZ031pI2xs.vxvDIBKickxm', 'jilasizwegigolo@gmail.com', 'Yes', NULL, 'No'),
(6, '0823126044', '$2y$10$Oo2mltcwWLLbckh8VhF.UOCYuR5pTUbqzpNlsxJbGp9J8VxualRb.', 'dummyemail@gmail.com', 'Yes', NULL, 'No'),
(7, '0712321122', '$2y$10$ygqsb3uNSn14WKMnH1kA3e.onghgFM6WyB0B4JlQ2ECA/S9MqhOCi', 'johnnydoe@gmail.com', 'Yes', NULL, 'No'),
(8, '0737656126', '$2y$10$kNZwHFW4gw5qSCAWknD9juHLyONkUby2cJVUZS1mdt9V6rQk/8sCK', 'test@gmail.com', 'Yes', NULL, 'No'),
(9, '0813109193', '$2y$10$NLxicwQ7Nl.jpgn1cf7c/OHlgSDdbBLChHESaf59.M3lhTh31reji', 'edwardson@gmail.com', 'Yes', NULL, 'No'),
(10, '0843156594', '$2y$10$uyXbbq33qr0DrJVCZqjnKeSMlYMWyit8wCTJynqlNE0aEhyMyLc92', 'mesuli@focalsoft.co.za', 'Yes', NULL, 'No'),
(11, '0719442777', '$2y$10$SB.C5XmvcACWPepuF4ZBXO21plBIDHxDek3U/DC5Lh.A4iKC0qTVC', 'edward@nobuhlemedia.co.za', 'Yes', NULL, 'No'),
(12, '0723126040', '$2y$10$1S5gPsyIQlEWzjfu/NIjgetV1KSefMjlJi1cwnqnXRQxCge/F0vYW', 'annastac@gmail.com', 'Yes', NULL, 'No'),
(13, '0893109193', '$2y$10$0MMM9HU/zMu2EDFtpEZg6.HnPceoD2K.o0ZCTMraeBuFkxoCsKSRu', 'ukhozi@gmail.com', 'Yes', NULL, 'No'),
(14, '0653768442', '$2y$10$rJ5Jit0YGRM8KlxgOZgb1.K4Az7v1wI0dG/ZXsEeUU8Ie8lgECK02', 'edwards@gmails.com', 'Yes', NULL, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `id` int(11) NOT NULL,
  `department` varchar(225) NOT NULL,
  `subject` varchar(225) NOT NULL,
  `message` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `owner` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`id`, `department`, `subject`, `message`, `status`, `owner`) VALUES
(1, 'Technical Support', 'Customer Support', 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type.', 'open', '0738836795'),
(2, 'Technical Support', 'Support Ticket 2', 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type.', 'open', '0738836795'),
(3, 'Technical Support', 'dasdsaads', 'adsadasadsasdsad', 'open', '0723126044'),
(4, 'Technical Support', 'This is an example', 'This is an example support ticket for testing purposes.', 'open', '0812762691'),
(5, 'Billing', 'This is an example', 'This is an example This is an example This is an example This is an example This is an example This is an example This is an example This is an example.', 'open', '0812762691');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `item_title` varchar(225) NOT NULL,
  `item_category` varchar(225) NOT NULL,
  `item_price` varchar(225) NOT NULL,
  `payment_type` varchar(225) NOT NULL,
  `item_gallery` varchar(225) NOT NULL,
  `owner` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_data`
--

CREATE TABLE `user_data` (
  `id` int(11) NOT NULL,
  `first_name` varchar(225) NOT NULL,
  `last_name` varchar(225) NOT NULL,
  `cell` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `province` varchar(225) NOT NULL,
  `city` varchar(225) NOT NULL,
  `owner` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user_data`
--

INSERT INTO `user_data` (`id`, `first_name`, `last_name`, `cell`, `email`, `province`, `city`, `owner`) VALUES
(5, 'Sizwe', 'Jila', '0812762691', 'edwardthabekhulu@gmail.com', 'KwaZulu-Natal', 'Durban', '0812762691'),
(7, 'Lihle', 'Mpanza', '0737656127', 'annamacave@gmail.com', 'KwaZulu-Natal', 'Durban', '0737656127'),
(8, 'Albino', 'Mahumana', '0731112344', 'mralbinomahumana@gmail.com', 'KwaZulu-Natal', 'Durban', '0731112344'),
(9, 'Sizwe', 'Jila', '0738836795', 'jilasizwegigolo@gmail.com', 'KwaZulu-Natal', 'Durban', '0738836795'),
(10, 'lindelani', 'mbhele', '0780728001', '', '', '', ''),
(11, 'latiswa', 'mthethwa', '0655607233', '', '', '', ''),
(12, 'mzwandile', 'sikhosana', '0765216010', '', '', '', ''),
(13, 'do ah', 'ndlovu', '824398295', '', '', '', ''),
(14, 'sakhayedwa', 'ndlovu', '780708647', '', '', '', ''),
(15, 'sindiswa', 'zondi', '680715986', '', '', '', ''),
(16, 'majid', 'khan', '614939385', '', '', '', ''),
(17, 'kresin', 'manival', '646876224', '', '', '', ''),
(18, 'john mbuyisa', 'mpanza', '734197014', '', '', '', ''),
(19, 'bridget', 'heslop', '736640334', '', '', '', ''),
(20, 'arthur', 'luwaca', '735441555', '', '', '', ''),
(21, 'ncami', 'ndlamini', '834322384', '', '', '', ''),
(22, 'zethu', 'khubisa', '760652180', '', '', '', ''),
(23, 'thembisa', 'mtiya', '721060068', '', '', '', ''),
(24, 'thembinkosi', 'khubisa', '787673833', '', '', '', ''),
(25, 'sthembiso', 'mjwara', '825168256', '', '', '', ''),
(26, 'octavia', 'ndabezitha', '844191972', '', '', '', ''),
(27, 'phikiwe', 'nxumalo', '814090671', '', '', '', ''),
(28, 'ncamsile', 'hlongwane', '723378034', '', '', '', ''),
(29, 'thando', 'makhoba', '768160645', '', '', '', ''),
(30, 'bongani', 'nongoma', '723604159', '', '', '', ''),
(31, 'elizabeth', 'siya', '786620406', '', '', '', ''),
(32, 'themba daniel', 'mtetwa', '722184180', '', '', '', ''),
(33, 'thulani', 'mpanza', '618692995', '', '', '', ''),
(34, 'Mohammed', 'Karrim', '623747956', '', '', '', ''),
(35, 'mandla', 'khoza', '837713713', '', '', '', ''),
(36, 'Edward', 'Mpanza', '0723126044', 'edward@focalsoft.co.za', 'KwaZulu-Natal', 'Durban', '0723126044'),
(37, 'John', 'Doe', '0712321122', 'johnnydoe@gmail.com', 'KwaZulu-Natal', 'Durban', '0712321122'),
(38, 'John', 'Doe', '0737656126', 'test@gmail.com', 'Gauteng', 'Durban', '0737656126'),
(39, 'Edward', 'Mpanza', '0813109193', 'edwardson@gmail.com', 'KwaZulu-Natal', 'Durban', '0813109193'),
(40, 'Mesuli', 'Mthembu', '0843156594', 'mesuli@focalsoft.co.za', 'KwaZulu-Natal', 'Durban', '0843156594'),
(41, 'Edward', 'Mpanza', '0719442777', 'edward@nobuhlemedia.co.za', 'KwaZulu-Natal', 'Durban', '0719442777'),
(42, 'Linda', 'Sibiya', '0893109193', 'ukhozi@gmail.com', 'KwaZulu-Natal', 'Durban', '13'),
(43, 'John', 'Doe', '0653768442', 'edwards@gmails.com', 'KwaZulu-Natal', 'Durban', '14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_view`
--
ALTER TABLE `business_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `compliance`
--
ALTER TABLE `compliance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dp`
--
ALTER TABLE `dp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_view`
--
ALTER TABLE `item_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`memberID`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_data`
--
ALTER TABLE `user_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `business_view`
--
ALTER TABLE `business_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `compliance`
--
ALTER TABLE `compliance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `dp`
--
ALTER TABLE `dp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `item_view`
--
ALTER TABLE `item_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `memberID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_data`
--
ALTER TABLE `user_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
