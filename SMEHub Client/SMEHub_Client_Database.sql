-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 17, 2022 at 01:53 PM
-- Server version: 10.3.37-MariaDB
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `focalsoft_smehub`
--

-- --------------------------------------------------------

--
-- Table structure for table `st_accounts`
--

CREATE TABLE `st_accounts` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `id_number` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `bname` varchar(255) NOT NULL,
  `bcontact` varchar(255) NOT NULL,
  `ppic` varchar(255) NOT NULL,
  `cpic` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `st_accounts`
--

INSERT INTO `st_accounts` (`id`, `email`, `password`, `fullname`, `id_number`, `city`, `bname`, `bcontact`, `ppic`, `cpic`, `type`, `description`) VALUES
(1, 'kwanele@focalsoft.co.za', 'd68229f521316a03b517e42128c87060', 'Kwanele Mngoma', '91100958070823', 'Durban', 'Nobuhle Media', '0827568442', '3caefcf499f3b4801fca514aeb544423.jpg', '8415cf776657c6eeba1e550dbe84e6da.jpg', 'product', ''),
(2, 'lwandle@gmail.com', 'd68229f521316a03b517e42128c87060', 'Lwandle Ndende', '92938973933', 'New Germany', 'Ndende Florists', '0783737732', '85d84e470d8ce7fbf863e296cd5960cb.png', '76ebb9069d020a2bf842e169303312bd.jpg', 'product', ''),
(3, 'xolani@gmail.com', 'd68229f521316a03b517e42128c87060', 'Praise Xolani', '91100958098', 'Phoenix', 'Nobuhle Media', '073847747', '1c52745b5e5132fc625e2b85c7bf04a7.jpg', 'a83b3dbf4c7ffd61195b953fbbe89bf6.jpg', 'service', ''),
(4, 'zamani@gmail.com', 'd68229f521316a03b517e42128c87060', 'Zamani Khawuke', '9108308333', 'Durban', 'Ekhishini Catering', '0827568445', '8f5a5fcbd79e8b6ac896f132d7ea4356.jpg', '6f95c9ea3004c34a42266c789262b95d.jpg', 'product', '');

-- --------------------------------------------------------

--
-- Table structure for table `st_entities`
--

CREATE TABLE `st_entities` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `identity_no` varchar(255) NOT NULL,
  `ck` varchar(255) NOT NULL,
  `physical_address` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `ps_list` varchar(255) NOT NULL,
  `offering` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `agent` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `st_items`
--

CREATE TABLE `st_items` (
  `id` int(15) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `flavour` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `size` varchar(250) NOT NULL,
  `category` varchar(255) NOT NULL,
  `visibility` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `likes` int(15) NOT NULL,
  `price` varchar(255) NOT NULL,
  `charge_type` varchar(255) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `sold` int(11) NOT NULL,
  `status` varchar(250) NOT NULL,
  `views` int(11) NOT NULL,
  `last_view` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `item_type` varchar(255) NOT NULL,
  `owner` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `st_items`
--

INSERT INTO `st_items` (`id`, `name`, `description`, `flavour`, `color`, `size`, `category`, `visibility`, `image`, `likes`, `price`, `charge_type`, `ref`, `sold`, `status`, `views`, `last_view`, `date`, `item_type`, `owner`) VALUES
(1, 'Florez Plant', '', '', '', '', 'Florists', '', '6af8525253616ea69e93034999ea74da.jpg', 0, '245', 'once-off', '9d7726fec0bac213d2fe59724c4e924e', 0, '', 0, '', '', 'product', '1'),
(2, 'Yellow Florist', '', '', '', '', 'Florists', '', 'ee4540f075daeac7e7c084c345d6cc3f.jpg', 0, '244', 'once-off', 'ee4540f075daeac7e7c084c345d6cc3f', 0, '', 0, '', '', 'product', '2'),
(3, 'Fuuz Plant', '', '', '', '', 'Florists', '', 'e7ecc931812b879e78b33b628acf3226.jpg', 0, '255', 'once-off', 'e7ecc931812b879e78b33b628acf3226', 0, '', 0, '', '', 'product', '2'),
(4, 'Search Engine Optomisation', '', '', '', '', 'Website Designers', '', '4a6501e902cf0168f5c319d55ee87850.jpg', 0, '560', 'per-hour', '4a6501e902cf0168f5c319d55ee87850', 0, '', 0, '', '', 'service', '3'),
(5, 'Videography', '', '', '', '', 'Website Designers', '', 'a67ba158b464373e1bf43df935bf1554.jpg', 0, '670', 'once-off', 'a67ba158b464373e1bf43df935bf1554', 0, '', 0, '', '', 'service', '3'),
(6, 'Administration ', '', '', '', '', 'Website Designers', '', 'df60271511c8ad46569017e90470176e.jpg', 0, '534', 'per-hour', 'df60271511c8ad46569017e90470176e', 0, '', 0, '', '', 'service', '3'),
(7, 'FInance Consulting', '', '', '', '', 'Website Designers', '', 'dafcedb55a31974a135417366319ff5b.jpg', 0, '3500', 'once-off', 'dafcedb55a31974a135417366319ff5b', 0, '', 0, '', '', 'service', '3'),
(8, 'Music Mastering', '', '', '', '', 'Website Designers', '', 'cd6e97e891756cc0e04f2149cd89480c.jpeg', 0, '678', 'once-off', 'cd6e97e891756cc0e04f2149cd89480c', 0, '', 0, '', '', 'service', '3'),
(9, 'Fried Crisps', '', '', '', '', 'Caterers', '', '3c206e846e84b37c26a6fcfa64d68efb.jpg', 0, '230', 'once-off', '3c206e846e84b37c26a6fcfa64d68efb', 0, '', 0, '', '', 'product', '4'),
(10, 'Delux Platter', '', '', '', '', 'Caterers', '', '51e13809190583df422e3b5e69dac4a4.jpg', 0, '574', 'once-off', '51e13809190583df422e3b5e69dac4a4', 0, '', 0, '', '', 'product', '4'),
(11, 'Brownie Delux', '', '', '', '', 'Caterers', '', '5bf5304ad5d6f8149b9fc509a33839bf.jpg', 0, '90', 'once-off', '5bf5304ad5d6f8149b9fc509a33839bf', 0, '', 0, '', '', 'product', '4'),
(12, 'Cake Platter', '', '', '', '', '678', '', '57fa76443e980731678087b7e831e06f.jpg', 0, '780', 'once-off', '57fa76443e980731678087b7e831e06f', 0, '', 0, '', '', 'product', '4');

-- --------------------------------------------------------

--
-- Table structure for table `st_items_services`
--

CREATE TABLE `st_items_services` (
  `id` int(15) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `flavour` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `size` varchar(250) NOT NULL,
  `category` varchar(255) NOT NULL,
  `visibility` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `likes` int(15) NOT NULL,
  `price` varchar(255) NOT NULL,
  `charge_type` varchar(255) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `sold` int(11) NOT NULL,
  `status` varchar(250) NOT NULL,
  `views` int(11) NOT NULL,
  `last_view` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `item_type` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `st_location`
--

CREATE TABLE `st_location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `province` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `st_location`
--

INSERT INTO `st_location` (`id`, `name`, `province`) VALUES
(1, 'Adams Mission', 1),
(2, 'Alverstone', 1),
(3, 'Amajuba District', 1),
(4, 'Amanzimtoti', 1),
(5, 'Anerley', 1),
(6, 'Arena Park', 1),
(7, 'Assagay', 1),
(8, 'Athlone Park', 1),
(9, 'Avoca', 1),
(10, 'Babanango', 1),
(11, 'Balgowan', 1),
(12, 'Ballito', 1),
(13, 'Bazley Beach', 1),
(14, 'Bellair', 1),
(15, 'Berea', 1),
(16, 'Berea West', 1),
(17, 'Bergville', 1),
(18, 'Bluff', 1),
(19, 'Boston', 1),
(20, 'Braemar', 1),
(21, 'Bulwer', 1),
(22, 'Cato Manor', 1),
(23, 'Cato Ridge', 1),
(24, 'Chatsworth', 1),
(25, 'Chiltern Hills', 1),
(26, 'Clermont', 1),
(27, 'Cliffdale', 1),
(28, 'Clifton', 1),
(29, 'Colenso', 1),
(30, 'Cowies Hill', 1),
(31, 'Crestholm', 1),
(32, 'Dalton', 1),
(33, 'Dannhauser', 1),
(34, 'Dassenhoek', 1),
(35, 'Drummond', 1),
(36, 'Dududu', 1),
(37, 'Dundee', 1),
(38, 'Durban', 1),
(39, 'Durban CBD', 1),
(40, 'Durban North', 1),
(41, 'ekuPhakameni', 1),
(42, 'Elandslaagte', 1),
(43, 'Emberton', 1),
(44, 'Empangeni', 1),
(45, 'Escombe', 1),
(46, 'Eshowe', 1),
(47, 'Essenwood', 1),
(48, 'Estcourt', 1),
(49, 'eThekwini District', 1),
(50, 'Ezinqoleni', 1),
(51, 'Forest Hills', 1),
(52, 'Franklin', 1),
(53, 'Gillitts', 1),
(54, 'Glen Park', 1),
(55, 'Glencoe', 1),
(56, 'Glenwood', 1),
(57, 'Greytown', 1),
(58, 'Greyville', 1),
(59, 'Gillitts', 1),
(60, 'Hammarsdale', 1),
(61, 'Harding', 1),
(62, 'Harding', 1),
(63, 'Harrison', 1),
(64, 'Harry Gwala District', 1),
(65, 'Hattingspruit', 1),
(66, 'Hibberdene', 1),
(67, 'Hillary', 1),
(68, 'Hillcrest', 1),
(69, 'Hilton', 1),
(70, 'Himeville', 1),
(71, 'Hluhluwe', 1),
(72, 'Howick', 1),
(73, 'Ifafa Beach', 1),
(74, 'iLembe District', 1),
(75, 'iLlovu', 1),
(76, 'Inanda', 1),
(77, 'Inchanga', 1),
(78, 'Ingwavuma', 1),
(79, 'Isipingo', 1),
(80, 'Ixopo', 1),
(81, 'Jozini', 1),
(82, 'Kelso', 1),
(83, 'King Cetshwayo', 1),
(84, 'Kingsburgh', 1),
(85, 'Klaarwater', 1),
(86, 'Kloof', 1),
(87, 'Kokstad', 1),
(88, 'Kranskop', 1),
(89, 'KwaDabeka', 1),
(90, 'KwaDukuza', 1),
(91, 'KwaMakhutha', 1),
(92, 'KwaMashu', 1),
(93, 'KwaNdengezi', 1),
(94, 'La Lucia', 1),
(95, 'La Mercy', 1),
(96, 'Ladysmith', 1),
(97, 'Louwsburg', 1),
(98, 'Madadeni', 1),
(99, 'Mahlabatini', 1),
(100, 'Malvern', 1),
(101, 'Mandeni', 1),
(102, 'Manguzi', 1),
(103, 'Manor Gardens', 1),
(104, 'Maphumulo', 1),
(105, 'Margate', 1),
(106, 'Marianridge', 1),
(107, 'Marianhill', 1),
(108, 'Maryvale', 1),
(109, 'Mayville', 1),
(110, 'Mbazwana', 1),
(111, 'Melmoth', 1),
(112, 'Melville', 1),
(113, 'Merebank', 1),
(114, 'Merrivale', 1),
(115, 'Mkuze', 1),
(116, 'Mobeni', 1),
(117, 'Montclair', 1),
(118, 'Mooi River', 1),
(119, 'Moseley', 1),
(120, 'Mount Edgecombe', 1),
(121, 'Mpumalanga', 1),
(122, 'Mtubatuba', 1),
(123, 'Mtunzini', 1),
(124, 'Mtwalume', 1),
(125, 'Munster', 1),
(126, 'Nazareth', 1),
(127, 'New Germany', 1),
(128, 'New Hanover', 1),
(129, 'New Castle', 1),
(130, 'Nkandla', 1),
(131, 'Nongoma', 1),
(132, 'Northdene', 1),
(133, 'Nquthu', 1),
(134, 'Ntuzuma', 1),
(135, 'Osizweni', 1),
(136, 'Overport', 1),
(137, 'Palm Beach', 1),
(138, 'Park Rynie', 1),
(139, 'Paulpietersburg', 1),
(140, 'Pennington', 1),
(141, 'Phoenix', 1),
(142, 'Pietermaritzburg', 1),
(143, 'Pinetown', 1),
(144, 'Point Waterfront', 1),
(145, 'Pomeroy', 1),
(146, 'Pongola', 1),
(147, 'Port Edward', 1),
(148, 'Port Shepstone', 1),
(149, 'Prospecton', 1),
(150, 'Queensburgh', 1),
(151, 'Reservoir Hills', 1),
(152, 'Reunion', 1),
(153, 'Ramsgate', 1),
(154, 'Richards Bay', 1),
(155, 'Richmond', 1),
(156, 'Rossburgh', 1),
(157, 'Salt Rock', 1),
(158, 'Sarnia', 1),
(159, 'Savannah Park', 1),
(160, 'Scottburgh', 1),
(161, 'Seaview', 1),
(162, 'Sezela', 1),
(163, 'Shallcross', 1),
(164, 'Shelly Beach', 1),
(165, 'Sherwood', 1),
(166, 'Shongweni', 1),
(167, 'Southbroom', 1),
(168, 'Springfield', 1),
(169, 'St Wendolins Ridge', 1),
(170, 'Stamford Hill', 1),
(171, 'Summerveld', 1),
(172, 'Sydenham', 1),
(173, 'Thongathi', 1),
(174, 'Thornwood', 1),
(175, 'Wentworth', 1),
(176, 'Ubombo', 1),
(177, 'Ugu District', 1),
(178, 'Ulundi', 1),
(179, 'Umbilo', 1),
(180, 'Umbumbulu', 1),
(181, 'Umdloti', 1),
(182, 'Umgababa', 1),
(183, 'uMgungundlovu District', 1),
(184, 'uMhlanga', 1),
(185, 'uMhlathuzana', 1),
(186, 'UMkhanyakude District', 1),
(187, 'Umkomaas', 1),
(188, 'Umlazi', 1),
(189, 'Umtentweni', 1),
(190, 'Umzimkulu', 1),
(191, 'Umzinto', 1),
(192, 'Umzinyathi District', 1),
(193, 'Umzumbe', 1),
(194, 'Underberg', 1),
(195, 'UThukela District', 1),
(196, 'Uvongo', 1),
(197, 'Verulam', 1),
(198, 'Vryheid', 1),
(199, 'Wartburg', 1),
(200, 'Wasbank', 1),
(201, 'Waterfall', 1),
(202, 'Welbedacht', 1),
(203, 'Westbrook', 1),
(204, 'Westwood', 1),
(205, 'Westville', 1),
(206, 'Weza', 1),
(207, 'Windermere', 1),
(208, 'Winston Park', 1),
(209, 'Woodhaven', 1),
(210, 'Woodlands', 1),
(211, 'Wyebank', 1),
(212, 'Yellowwood Park', 1),
(213, 'Zululand', 1),
(214, 'Stanger', 1);

-- --------------------------------------------------------

--
-- Table structure for table `st_orders`
--

CREATE TABLE `st_orders` (
  `id` int(11) NOT NULL,
  `ref_no` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `dtime` varchar(255) NOT NULL,
  `owner` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `st_provinces`
--

CREATE TABLE `st_provinces` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `st_provinces`
--

INSERT INTO `st_provinces` (`id`, `name`) VALUES
(1, 'KwaZulu Natal'),
(2, 'Gauteng'),
(3, 'Mpumalanga'),
(4, 'Eastern Cape'),
(5, 'Western Cape'),
(6, 'Northen Cape'),
(7, 'Limpopo'),
(8, 'Free State'),
(9, 'North West');

-- --------------------------------------------------------

--
-- Table structure for table `st_sp_category`
--

CREATE TABLE `st_sp_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `st_sp_category`
--

INSERT INTO `st_sp_category` (`id`, `name`, `description`) VALUES
(1, 'Accommodation & Venues', ''),
(2, 'Agricultural', ''),
(3, 'Alarms & Security', ''),
(4, 'Architecture & Engineering', ''),
(5, 'Business & Finance', ''),
(6, 'Cars & Automotive', ''),
(7, 'Computers & Telecommunications', ''),
(8, 'Digital Design, Media & Marketing', ''),
(9, 'Family Care', ''),
(10, 'Fashion, Tailors & Outfitters', ''),
(11, 'Food & Beverage', ''),
(12, 'Health Supplies & Equipment', ''),
(13, 'Health, Wellness & Beauty', ''),
(14, 'Heavy Equipment & Machinery', ''),
(15, 'Home Appliances & Equipment', ''),
(16, 'Home, Building & Gardening', ''),
(17, 'Lessons & Training', ''),
(18, 'Manufacturers & Suppliers', ''),
(19, 'Pets', ''),
(20, 'Property & Legal', ''),
(21, 'Transportation & Logistics', ''),
(22, 'Tourism & Outdoor Activities', ''),
(23, 'Weddings, Events & Entertainment', '');

-- --------------------------------------------------------

--
-- Table structure for table `st_sp_list`
--

CREATE TABLE `st_sp_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `st_sp_list`
--

INSERT INTO `st_sp_list` (`id`, `name`, `category`) VALUES
(1, 'Djs', 23),
(2, 'Event Decorations', 23),
(3, 'Event Planners', 23),
(4, 'Florists', 23),
(5, 'Party Planners', 23),
(6, 'Photographers', 23),
(7, 'Tent Hire', 23),
(8, 'Toilet Hire', 23),
(9, 'Videographers', 23),
(10, 'Wedding', 23),
(11, 'Photographers', 23),
(12, 'Holiday Accommodation', 1),
(13, 'Venues', 1),
(14, 'Wedding Venues', 1),
(15, 'Agricultural Equipment', 2),
(16, 'Agricultural Services', 2),
(17, 'Irrigation', 2),
(18, 'Access Control', 3),
(19, 'Alarm Systems', 3),
(20, 'Armed Response', 3),
(21, 'Car Alarms', 3),
(22, 'Car Tracking', 3),
(23, 'CCTV', 3),
(24, 'Fire Safety', 3),
(25, 'Private Investigators', 3),
(26, 'Security', 3),
(27, 'Tracing', 3),
(28, 'Architects', 4),
(29, 'Borehole Drillers', 4),
(30, 'Town Planners', 4),
(31, 'Accounting', 5),
(32, 'Auditors', 5),
(33, 'Business Consultants', 5),
(34, 'Company Registrations', 5),
(35, 'Debt Collection', 5),
(36, 'Debt Counsellors', 5),
(37, 'Financial Advisors', 5),
(38, 'InsuranceLoans', 5),
(39, 'Medical Aid', 5),
(40, 'Recruitment Agencies', 5),
(41, 'Auto Electricians', 6),
(42, 'Auto Glass', 6),
(43, 'Batteries', 6),
(44, 'Brakes and Clutches', 6),
(45, 'Car Aircon Regassing', 6),
(46, 'Car Window Tinting', 6),
(47, 'Engine Overhauls', 6),
(48, 'Fuels', 6),
(49, 'Gearboxes', 6),
(50, 'Mechanics', 6),
(51, 'Panel Beaters', 6),
(52, 'Towing', 6),
(53, 'Tow Bars', 6),
(54, 'Apple Repairs', 7),
(55, 'Cellphone Repairs', 7),
(56, 'Computers', 7),
(57, 'Computer Repairs', 7),
(58, 'Internet Solutions', 7),
(59, 'Laptop Repairs', 7),
(60, 'Networking', 7),
(61, 'Office Equipment', 7),
(62, 'Printer Cartridges', 7),
(63, 'Graphic Designers', 8),
(64, 'Logo Design', 8),
(65, 'Printing', 8),
(66, 'Signs', 8),
(67, 'Website Designers', 8),
(68, 'Baby Sitters', 9),
(69, 'Creches', 9),
(70, 'Day Care Centres', 9),
(71, 'Pre-Schools', 9),
(72, 'Dressmakers', 10),
(73, 'Embroidery', 10),
(74, 'Tailoring', 10),
(75, 'Birthday Cakes', 11),
(76, 'Cake Shops', 11),
(77, 'Caterers', 11),
(78, 'Hygiene Equipment', 12),
(79, 'Medical Equipment', 12),
(80, 'Medical Supplies', 12),
(81, 'Beauty Salons', 13),
(82, 'Chiropractors', 13),
(83, 'Counsellors', 13),
(84, 'Dentists', 13),
(85, 'Dermatologists', 13),
(86, 'Dieticians', 13),
(87, 'Gynaecologists', 13),
(88, 'Hair Stylists', 13),
(89, 'Laser Clinics', 13),
(90, 'Make Up Artists', 13),
(91, 'Marriage Counsellors', 13),
(92, 'Massage Therapists', 13),
(93, 'Personal Trainers', 13),
(94, 'Physiotherapists', 13),
(95, 'Plastic Surgeons', 13),
(96, 'Psychologists', 13),
(97, 'Generators', 14),
(98, 'Plant Hire', 14),
(99, 'Skip Hire', 14),
(100, 'Tlb Hire', 14),
(101, 'Appliance Repairs', 15),
(102, 'DSTV Installers', 15),
(103, 'Fridge Repairs', 15),
(104, 'Tv Installers', 15),
(105, 'Tv Repairs', 15),
(106, 'Air Conditioning', 16),
(107, 'Aluminium Doors and Windows', 16),
(108, 'Awnings', 16),
(109, 'Balustrades', 16),
(110, 'Bathroom Renovations', 16),
(111, 'Blinds', 16),
(112, 'Builders', 16),
(113, 'Burglar Bars', 16),
(114, 'Carpenters', 16),
(115, 'Carpeting', 16),
(116, 'Carpet Cleaning', 16),
(117, 'Carports', 16),
(118, 'Ceiling Installers', 16),
(119, 'Cleaning Materials', 16),
(120, 'Cleaning Services', 16),
(121, 'Concrete Slabs', 16),
(122, 'Curtains', 16),
(123, 'Doors', 16),
(124, 'Drywalls', 16),
(125, 'Electricians', 16),
(126, 'Electric Fencing', 16),
(127, 'Fencing', 16),
(128, 'Flooring', 16),
(129, 'Garage Doors', 16),
(130, 'Garage Door Motors', 16),
(131, 'Gardeners', 16),
(132, 'Gas Installers', 16),
(133, 'Gates', 16),
(134, 'Gate Motors', 16),
(135, 'Glass Works', 16),
(136, 'Guttering', 16),
(137, 'Handymen', 16),
(138, 'High Pressure Cleaning', 16),
(139, 'Home Improvements', 16),
(140, 'Interior Designing', 16),
(141, 'Kitchen Renovations', 16),
(142, 'Laminate Flooring', 16),
(143, 'Landscaping', 16),
(144, 'Laundry Services', 16),
(145, 'Locksmiths', 16),
(146, 'Office Cleaning', 16),
(147, 'Painters', 16),
(148, 'Palisade Fencing', 16),
(149, 'Paving', 16),
(150, 'Pest Control', 16),
(151, 'Plastering', 16),
(152, 'Plumbers', 16),
(153, 'Pool Cleaning', 16),
(154, 'Precast Fencing', 16),
(155, 'Prepaid Electricity Meters', 16),
(156, 'Roofing', 16),
(157, 'Security Gates', 16),
(158, 'Shadeports', 16),
(159, 'Shower Doors', 16),
(160, 'Solar Geysers', 16),
(161, 'Solar Systems', 16),
(162, 'Swimming Pool Builders', 16),
(163, 'Tar Surfacing', 16),
(164, 'Thatched Roofing', 16),
(165, 'Tiling', 16),
(166, 'Tree Felling', 16),
(167, 'Upholsterers', 16),
(168, 'Upholstery Cleaning', 16),
(169, 'Waterproofing', 16),
(170, 'Welders', 16),
(171, 'Wendy Houses', 16),
(172, 'Window Cleaning', 16),
(173, 'Window Tinting', 16),
(174, 'Wire Mesh Fencing', 16),
(175, 'Wooden Decking', 16),
(176, 'Computer Courses', 17),
(177, 'Driving Schools', 17),
(178, 'Firearm Training', 17),
(179, 'First Aid', 17),
(180, 'Forklift Training', 17),
(181, 'Life Coaches', 17),
(182, 'Make Up Courses', 17),
(183, 'Security Training', 17),
(184, 'Sewing and Fashion Lessons', 17),
(185, 'Swimming Lessons', 17),
(186, 'Team Building', 17),
(187, 'Building Materials', 18),
(188, 'Gas Suppliers', 18),
(189, 'Marble and Granite Suppliers', 18),
(190, 'Personal Protection Equipment', 18),
(191, 'Stationery', 18),
(192, 'Swimming Pool Supplies', 18),
(193, 'Dog Training', 19),
(194, 'Groomers', 19),
(195, 'Pet Sitters', 19),
(196, 'Conveyancers', 20),
(197, 'Divorce Lawyers', 20),
(198, 'Estate Agents', 20),
(199, 'Labour Lawyers', 20),
(200, 'Lawyers', 20),
(201, 'Couriers', 21),
(202, 'Movers', 21),
(203, 'Rubble Removal', 21),
(204, 'School Transport', 21),
(205, 'Shuttle Services', 21),
(206, 'Taxis', 21),
(207, 'Transportation', 21),
(208, 'Fishing Charters', 22),
(209, 'Tour Operators', 22),
(210, 'Travel Agents', 22);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `st_accounts`
--
ALTER TABLE `st_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_entities`
--
ALTER TABLE `st_entities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_items`
--
ALTER TABLE `st_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_items_services`
--
ALTER TABLE `st_items_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_location`
--
ALTER TABLE `st_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_orders`
--
ALTER TABLE `st_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_provinces`
--
ALTER TABLE `st_provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_sp_category`
--
ALTER TABLE `st_sp_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_sp_list`
--
ALTER TABLE `st_sp_list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `st_accounts`
--
ALTER TABLE `st_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `st_entities`
--
ALTER TABLE `st_entities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_items`
--
ALTER TABLE `st_items`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `st_items_services`
--
ALTER TABLE `st_items_services`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_location`
--
ALTER TABLE `st_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;

--
-- AUTO_INCREMENT for table `st_orders`
--
ALTER TABLE `st_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_provinces`
--
ALTER TABLE `st_provinces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `st_sp_category`
--
ALTER TABLE `st_sp_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `st_sp_list`
--
ALTER TABLE `st_sp_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
